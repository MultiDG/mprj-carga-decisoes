package br.mp.mprj.cargadecisoes;

import br.mp.mprj.commonspdf.signning.Signature;
import br.mp.mprj.commonspdf.signning.SignatureExtractor;
import com.google.common.base.Strings;
import com.itextpdf.text.pdf.PdfReader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.text.Normalizer;
import java.util.List;

@Service
public class ValidaDocumento {

    @Autowired
    DecisaoQuery query;

    @Cacheable
    private PdfReader documento(byte[] documento) throws IOException {
        return new PdfReader(documento);
    }

    public boolean isDocumentoAssinado(byte[] documento, String numeroCNJLog) throws Exception {
        boolean is = false;
        PdfReader documentoLido = documento(documento);
        SignatureExtractor extractor;
        try {
         extractor= new SignatureExtractor(documentoLido);
        }catch (Exception e){
            throw new Exception("Erro ao tentar extrair a assinatura do documento");
        }

        List<String> magistrados = query.obterMagistrados();
        for (Signature assinatura : extractor.getSignatures()) {
            if(magistrados.contains(limparPalavra(assinatura.getSignatoryName()).toUpperCase())){
                is = true;
                break;
            }else {
                System.out.println("Documento do processo " + numeroCNJLog +" não está assinado por um magistrado\n");
                System.out.println("ou assinatura " + assinatura.getSignatoryName() +", " + assinatura.getSignatoryId() + " não consta na base.");
            }
        }
        return is;
    }
    public boolean isDocumentoComDescricaoDecisao(String descricao){
        boolean is = false;
        if(descricao != null && !Strings.isNullOrEmpty(descricao)){
            is = descricao.contains("Sentença") ||
                    descricao.contains("Decisão");
        }
        return is;
    }

    public boolean isDocumentoComTextoSentenca(byte[] documento) throws IOException {
        boolean is = false;
        PdfReader reader = documento(documento);
        String palavraLimpa;
        for (int i=1; i<=reader.getNumberOfPages(); i++){
            palavraLimpa = limparPalavra(com.itextpdf.text.pdf.parser.PdfTextExtractor.getTextFromPage(reader, i));
            if (palavraLimpa.contains("sentenca")){
                is = true;
                break;
            }
        }
        return is;
    }


    public String limparPalavra(String palavra){
        String palavraLimpa = null;
        if(palavra != null){
            palavraLimpa = Normalizer.normalize(palavra, Normalizer.Form.NFD)
                    .replaceAll("[^a-zA-Z0-9]", "")
                    .replaceAll("[^\\p{ASCII}]", "")
                    .toLowerCase();
        }
        return palavraLimpa;
    }
}
