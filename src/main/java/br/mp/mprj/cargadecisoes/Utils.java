package br.mp.mprj.cargadecisoes;

import br.mp.mprj.commons.model.DocumentoMni;
import org.joda.time.DateTime;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Service
public class Utils {

    public DocumentoMni getUltimoElementoLista(List<DocumentoMni> decisoes, DateTime dataDO){
        DocumentoMni ultimoDocumento = new DocumentoMni();
        if(decisoes != null && !decisoes.isEmpty()){
            List<Integer> posicoes = new ArrayList<>();
            for (DocumentoMni documento : decisoes) {
                if(documento.getDataHora().isBefore(dataDO.getMillis())){
                    posicoes.add(Integer.parseInt(documento.getNumeroFolhaVirtual()));
                }
            }
            Integer ultimaPosicao = Collections.max(posicoes);
            for (DocumentoMni documento : decisoes) {
                boolean isUltimo = documento.getNumeroFolhaVirtual()!= null &&
                    documento.getNumeroFolhaVirtual().equals(ultimaPosicao.toString());
                if(isUltimo){
                    ultimoDocumento = documento;
                    break;
                }
            }
        }
        return ultimoDocumento;
    }

    public DateTime recuperaDataDocumento(String filename) throws Exception{
        DateTime dataDiarioOficial = null;
        try{
            if(filename != null){
                int ano = Integer.parseInt(filename.substring(0, 4));
                int mes = Integer.parseInt(filename.substring(4, 6));
                int dia = Integer.parseInt(filename.substring(6, 8));
                dataDiarioOficial = new DateTime(ano, mes, dia, 0, 0);
            }
        }catch (Exception e){
            throw new Exception("Nome do arquivo está fora de padrão. Favor ajustar para o padrão: AAAAMMDDCAPDJETJRJ.pdf");
        }
       return dataDiarioOficial;
    }
}
