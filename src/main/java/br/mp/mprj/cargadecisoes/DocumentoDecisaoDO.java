package br.mp.mprj.cargadecisoes;

import br.mp.mprj.commons.*;
import br.mp.mprj.commons.exception.MniException;
import br.mp.mprj.commons.model.DocumentoMni;
import br.mp.mprj.commons.relational.ProcessoDiarioOficialRelational;
import br.mp.mprj.commons.ws.ObjectToSoapXml;
import br.mp.mprj.mni.tjrj.hml.v222.homologacao.ObjectFactory;
import br.mp.mprj.mni.tjrj.hml.v222.homologacao.TipoConsultarProcessoResposta;
import br.mp.mprj.mni.tjrj.hml.v222.homologacao.TipoParametro;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class DocumentoDecisaoDO {

    @Autowired
    ConsultarProcesso consultarProcesso;

    @Autowired
    ConverterMni toMgpe;

    @Autowired
    DecisaoQuery query;

    @Autowired
    RecuperaProcessos recuperaProcessos;

    @Autowired
    ValidaDocumento validaDocumento;

    @Autowired
    Utils util;

    @Autowired
    AmbienteConfig config;

    @Autowired
    TipoFimProcesso tipoFimProcesso;

    public void salvarDecisoes() throws Exception {
        Map<String, Integer> processos;
        String logMessage = null;
        try{
            System.out.println("Recuperando processos do arquivo..\n");
            switch (tipoFimProcesso.getTipo()){
                case Constants.PAR:
                    processos = recuperaProcessos.getPares(config.getCaminhoArquivo());
                    logMessage = "pares";
                    break;
                case Constants.IMPAR:
                    processos = recuperaProcessos.getImpares(config.getCaminhoArquivo());
                    logMessage = "impares";
                    break;
                default: processos = recuperaProcessos.getTodos(config.getCaminhoArquivo());
            }

            System.out.println(processos.size() +" processos " +logMessage+ " com sentença encontrados.\n");
        }catch (IOException e){
            throw new IOException("Arquivo não encontrado");
        }
        System.out.println("Filtrando intimados..\n");
        List<ProcessoDiarioOficialRelational> processosAsalvar = new ArrayList<>();
        Map<String, Integer> processosIntimados = new HashMap<>();
        int countIntimados = 0;
        for (Map.Entry<String, Integer> processo : processos.entrySet()) {
            if (query.isProcessoIntimado(processo.getKey())){
                ProcessoDiarioOficialRelational processoRelational =
                    ProcessoDiarioOficialRelational.builder()
                        .numeroCNJ(processo.getKey())
                        .dataDiarioOficial(new java.sql.Date(util.recuperaDataDocumento(config.getCaminhoArquivo()).getMillis()))
                        .capitalAnterior("C")
                        .isEletronico("E")
                        .pagina(processo.getValue())
                        .dataInclusao(new java.sql.Date(DateTime.now().getMillis()))
                        .build();
                processosIntimados.put(processo.getKey(), processo.getValue());
                processosAsalvar.add(processoRelational);
                System.out.println("Processo " + processo.getKey() + " intimado. Adicionando ao lote..\n");
                countIntimados++;
            }

        }
        System.out.println(countIntimados + " processos intimados\n");
        query.salvarMetadados(processosAsalvar);

        System.out.println("Processos salvos, Buscando conteúdo no MNI..\n");
        salvarConteudo(processosIntimados);
    }

    public void salvarConteudo(Map<String, Integer> processos) throws Exception {
        List<ProcessoDiarioOficialRelational> processosConteudoASalvar = new ArrayList<>();
        String meioTramitacao;
        String instancia;


        for (Map.Entry<String, Integer> processo : processos.entrySet()) {
            /**O nome do arquivo conterá a data que será usada como parametro de entrada no MNI*/
            TipoConsultarProcessoResposta resposta = consultarProcesso
                   .comDocumentosAPartirDe(processo.getKey(), util.recuperaDataDocumento(config.getCaminhoArquivo()).minusDays(90));

            /**Verifico se o processo é eletronico, por que caso ele não seja, não será atualizado.*/
            meioTramitacao = obeterMeioTramitacao(processo.getKey());
            instancia = obeterMeioInstancia(processo.getKey());
            List<DocumentoMni> decisoes = obterDecisoes(toMgpe.documentosMni(resposta));
            if(resposta.isSucesso()){
                 DocumentoMni decisao = util.getUltimoElementoLista(decisoes, util.recuperaDataDocumento(config.getCaminhoArquivo()));
                 processosConteudoASalvar.add(processoConteudoASalvar(decisao.getIdDocumento(), processo.getKey(), resposta, meioTramitacao, Integer.parseInt(instancia)));
            }else {
                throw new MniException("Problemas ao acessar o MNI: "+resposta.getMensagem()+"\nTente mais tarde");
            }
        }
        query.salvarConteudoEDadosMni(processosConteudoASalvar);

    }

    ProcessoDiarioOficialRelational processoConteudoASalvar(String idDocumento, String processo, TipoConsultarProcessoResposta resposta, String eletronico, Integer instancia) throws Exception {
        ObjectToSoapXml toXml = new ObjectToSoapXml();
        ObjectFactory factory = new ObjectFactory();
        ProcessoDiarioOficialRelational processoSalvo;
        if(eletronico.equals("E") && idDocumento != null){
            TipoConsultarProcessoResposta respostaConteudo = consultarProcesso.comConteudoDocumento(processo, idDocumento);
            if(respostaConteudo.isSucesso()){
                DocumentoMni documentoMni = toMgpe.documentoMni(respostaConteudo.getProcesso().getDocumento().get(0));
                boolean isValid = validaDocumento.isDocumentoAssinado(documentoMni.getConteudo(), processo);
//                       && validaDocumento.isDocumentoComTextoSentenca(documentoMni.getConteudo());
                if(isValid){
                    processoSalvo = ProcessoDiarioOficialRelational.builder()
                        .numeroCNJ(processo)
                        .descricaoMni(documentoMni.getDescricao())
                        .isEletronico(eletronico)
                        .dataInclusaoMni(new java.sql.Date(documentoMni.getDataHora().getMillis()))
                        .dataInclusaoDocumento(new java.sql.Date(DateTime.now().getMillis()))
                        .documento(documentoMni.getConteudo())
                        .instancia(instancia)
                        .build();
                }else {
                    processoSalvo = ProcessoDiarioOficialRelational.builder()
                        .numeroCNJ(processo)
                        .descricaoMni(documentoMni.getDescricao())
                        .isEletronico(eletronico)
                        .dataInclusaoMni(new java.sql.Date(documentoMni.getDataHora().getMillis()))
                        .xmlResponse(toXml.getXml(factory.createConsultarProcessoResposta(resposta)))
                        .documentoRejeitado(documentoMni.getConteudo())
                        .instancia(instancia)
                        .build();
                }
            }else {
                throw new MniException("Problemas ao acessar o MNI: "+resposta.getMensagem()+"\nTente mais tarde");
            }
        }else {
            processoSalvo = ProcessoDiarioOficialRelational.builder()
                .numeroCNJ(processo)
                .isEletronico(eletronico)
                .instancia(instancia)
                .build();
        }
        return processoSalvo;
    }

    List<DocumentoMni> obterDecisoes(List<DocumentoMni> documentoMniList) throws IOException {
        List<DocumentoMni> decisoes = new ArrayList<>();
        for (DocumentoMni documentoMni: documentoMniList) {
            boolean isDecisao = validaDocumento.isDocumentoComDescricaoDecisao(documentoMni.getDescricao());
            if (isDecisao) {
                decisoes.add(documentoMni);
            }
        }
        return decisoes;
    }

    private String obeterMeioTramitacao(String processo) throws Exception {
        TipoConsultarProcessoResposta respostaDadosBasicos = obterComDadosBasicos(processo);
        String eletronico = "E";
        for (TipoParametro outroParametro:respostaDadosBasicos.getProcesso().getDadosBasicos().getOutroParametro()) {
            if(outroParametro.getNome().equals(Constants.MEIO_TRAMITACAO)){
                eletronico = outroParametro.getValor();
                break;
            }
        }
        return eletronico;
    }

    private String obeterMeioInstancia(String processo) throws Exception {
        TipoConsultarProcessoResposta respostaDadosBasicos = obterComDadosBasicos(processo);
        String instancia = null;
        for (TipoParametro outroParametro:respostaDadosBasicos.getProcesso().getDadosBasicos().getOutroParametro()) {
            if(outroParametro.getNome().equals(Constants.INSTANCIA)){
                instancia = outroParametro.getValor();
                break;
            }
        }
        return instancia;
    }

    @Cacheable
    private TipoConsultarProcessoResposta obterComDadosBasicos(String processo) throws Exception {
        return consultarProcesso.comDadosBasicos(processo);
    }

}
