package br.mp.mprj.cargadecisoes;

import br.mp.mprj.commons.AmbienteConfig;
import br.mp.mprj.commons.relational.ProcessoDiarioOficialRelational;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

@Service
public class DecisaoQuery {

    @Autowired
    AmbienteConfig config;

    public boolean isProcessoIntimado(String numeroCNJ) throws Exception {
       StringBuilder sql = new StringBuilder();
       sql.append("select count(1) as quantidade ");
       sql.append("from MCPR.MCPR_DOCUMENTO d ");
       sql.append("where  1=1 ");
       sql.append("and d.docu_tpdc_dk = 116 ");
       sql.append("and d.docu_tpst_dk = 3 and D.DOCU_FSDC_DK = 1 ");
       sql.append("and D.DOCU_NR_EXTERNO = translate('"+numeroCNJ+"' ,'0123456789-.','0123456789')");
        PreparedStatement statement = null;
       try {
            statement= config.getConexao().prepareStatement(sql.toString());
       }catch (NullPointerException e){
           e.getMessage();
       }
       boolean isIntimado = false;
       if(statement != null){
           ResultSet set = statement.executeQuery();
           set.next();
           isIntimado = set.getInt("quantidade") > 0;
       }
       return isIntimado;
    }
    @Cacheable
    public List<String> obterMagistrados() throws Exception {
       List<String> magistrados= new ArrayList<>();
       String sql = "SELECT tjma_nm_corrigido FROM tjrj.tjrj_magistrado";
       PreparedStatement statement = config.getConexao().prepareStatement(sql);
       ResultSet set = statement.executeQuery();
       while (set.next()){
           magistrados.add(set.getString( "tjma_nm_corrigido"));
       }
       return magistrados;
    }

    public void salvarMetadados(List<ProcessoDiarioOficialRelational> processos) throws Exception{

        StringBuilder sql = new StringBuilder();

        sql.append("INSERT INTO tjrj.tjrj_decisoes_1instancia(");
        sql.append("tjpi_dk, ");
        sql.append("tjpi_processo, ");
        sql.append("tjpi_dt_diario_oficial, ");
        sql.append("tjpi_capital_interior, ");
        sql.append("tjpi_nr_pagina, ");
        sql.append("tjpi_dt_inclusao, ");
        sql.append("tjpi_tp_processo) ");
        sql.append("values(tjrj.tjrj_sq_tjpi_dk.nextval, ?, ?, ?, ?, ?, ?)");

        PreparedStatement statement = config.getConexao().prepareStatement(sql.toString());
        for (ProcessoDiarioOficialRelational processo : processos) {
            statement.setString(1, processo.getNumeroCNJ());
            statement.setDate(2, processo.getDataDiarioOficial());
            statement.setString(3, processo.getCapitalAnterior());
            statement.setInt(4, processo.getPagina());
            statement.setDate(5, new java.sql.Date(DateTime.now().getMillis()));
            statement.setString(6, processo.getIsEletronico());
            statement.executeUpdate();
        }
    }

    public void salvarConteudoEDadosMni(List<ProcessoDiarioOficialRelational> processos) throws Exception {

        StringBuilder sql = new StringBuilder();

        sql.append("UPDATE tjrj.tjrj_decisoes_1instancia SET ");
        sql.append("tjpi_descricao_mni = ?,");
        sql.append("tjpi_dt_documento_mni = ?,");
        sql.append("tjpi_documento = ?, ");
        sql.append("tjpi_dt_inclusao_blob = ?, ");
        sql.append("tjpi_xml_documento = ?, ");
        sql.append("tjpi_doc_rejeitado = ?, ");
        sql.append("tjpi_tp_processo = ?, ");
        sql.append("tjpi_nr_instancia = ? ");
        sql.append("where tjpi_processo = ?");

        PreparedStatement statement = config.getConexao().prepareStatement(sql.toString());

        for (ProcessoDiarioOficialRelational processo : processos) {
            statement.setString(1, processo.getDescricaoMni());
            statement.setDate(2, processo.getDataInclusaoMni());
            statement.setBytes(3, processo.getDocumento());
            statement.setDate(4, processo.getDataInclusaoDocumento());
            statement.setString(5, processo.getXmlResponse());
            statement.setBytes(6, processo.getDocumentoRejeitado());
            statement.setString(7, processo.getIsEletronico());
            statement.setInt(8, processo.getInstancia());
            statement.setString(9, processo.getNumeroCNJ());
            statement.executeUpdate();
        }

    }
}
