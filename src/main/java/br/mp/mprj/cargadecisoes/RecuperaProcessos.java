package br.mp.mprj.cargadecisoes;

import br.mp.mprj.commons.Constants;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.parser.PdfTextExtractor;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.*;

@Service
public class RecuperaProcessos {

    public Map<String, Integer> getTodos(String path) throws IOException {
        PdfReader documento = new PdfReader(path);
        Map<String, Integer> processosMap = new HashMap<>();
        for (int i=1; i<= documento.getNumberOfPages(); i++){
            String texto = new String(PdfTextExtractor.getTextFromPage(documento, i));
            List<String> paragrafos = Arrays.asList(texto.split("\n \n"));
            for (String paragrafo : paragrafos) {
                if(paragrafo.contains("Sentença:")){
                    java.util.regex.Matcher matcher = Constants.NUMEROCNJ_PATTERN.matcher(paragrafo);
                    if(matcher.find()){
                        processosMap.put(matcher.group(), i);
                    }
                }
            }
        }
        return processosMap;
    }


    public Map<String, Integer>getImpares(String path) throws IOException {
        Map<String, Integer> processosImpares = new HashMap<>();
        System.out.print("Recuperando apenas os processos impares\n");
        for (Map.Entry<String, Integer> processo : getTodos(path).entrySet()) {
            int numero = Character.getNumericValue(processo.getKey().charAt(6));
            if(numero % 2 != 0){
                processosImpares.put(processo.getKey(), processo.getValue());
            }
        }
        return processosImpares;

    }
    public Map<String, Integer> getPares(String path) throws IOException {
        Map<String, Integer> processosPares = new HashMap<>();
        System.out.print("Recuperando apenas os processos pares\n");
        for (Map.Entry<String, Integer> processo : getTodos(path).entrySet()) {

            int numero = Character.getNumericValue(processo.getKey().charAt(6));
            if(numero % 2 == 0){
                processosPares.put(processo.getKey(), processo.getValue());
            }
        }
        return processosPares;
    }
}
