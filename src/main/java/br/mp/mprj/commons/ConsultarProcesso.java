package br.mp.mprj.commons;

import br.mp.mprj.commons.ws.MniTjrjClient;
import br.mp.mprj.mni.tjrj.hml.v222.homologacao.ObjectFactory;
import br.mp.mprj.mni.tjrj.hml.v222.homologacao.TipoConsultarProcesso;
import br.mp.mprj.mni.tjrj.hml.v222.homologacao.TipoConsultarProcessoResposta;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.xml.bind.JAXBElement;

@Service
public class ConsultarProcesso {

    @Autowired
    MniTjrjClient mniTjrjClient;

    public TipoConsultarProcessoResposta comDocumentosAPartirDe(String numeroCNJ, DateTime dataReferencia) throws Exception{

        TipoConsultarProcesso tipoConsultarProcesso = new TipoConsultarProcesso();
        ObjectFactory objectFactory = new ObjectFactory();
        JAXBElement<Boolean> incluirDocumento = objectFactory.createTipoConsultarProcessoIncluirDocumentos(true);
        tipoConsultarProcesso.getDocumentoOrIncluirDocumentosOrIncluirCabecalho().add(incluirDocumento);
        tipoConsultarProcesso.setNumeroProcesso(numeroCNJ);
        tipoConsultarProcesso.setDataReferencia(dataReferencia.toString(Constants.MNI_DATE_PATTERN));

        return mniTjrjClient.consultarProcesso(tipoConsultarProcesso, true);
    }

    public TipoConsultarProcessoResposta comConteudoDocumento(String numeroCNJ, String idDocumento) throws Exception{

        TipoConsultarProcesso tipoConsultarProcesso = new TipoConsultarProcesso();
        ObjectFactory objectFactory = new ObjectFactory();
        JAXBElement<String> incluiConteudo = objectFactory.createTipoConsultarProcessoDocumento(idDocumento);
        tipoConsultarProcesso.getDocumentoOrIncluirDocumentosOrIncluirCabecalho().add(incluiConteudo);
        tipoConsultarProcesso.setNumeroProcesso(numeroCNJ);

        return mniTjrjClient.consultarProcesso(tipoConsultarProcesso, true);
    }

    public TipoConsultarProcessoResposta comDadosBasicos(String numeroCNJ) throws Exception{

        TipoConsultarProcesso tipoConsultarProcesso = new TipoConsultarProcesso();
        ObjectFactory objectFactory = new ObjectFactory();
        JAXBElement<Boolean> incluirCabecalho = objectFactory.createTipoConsultarProcessoIncluirCabecalho(true);
        tipoConsultarProcesso.getDocumentoOrIncluirDocumentosOrIncluirCabecalho().add(incluirCabecalho);
        tipoConsultarProcesso.setNumeroProcesso(numeroCNJ);

        return mniTjrjClient.consultarProcesso(tipoConsultarProcesso, true);
    }
}
