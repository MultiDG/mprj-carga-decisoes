package br.mp.mprj.commons.model;

import lombok.Builder;

@Builder
public class DocumentoDecisao {
    String numeroCNJ;
    String idDocumento;
    String tipoDocumentoLocal;
    String descricao;
    String identificadorDocumentoPai;
    String volume;
    String numeroFolhaVirtual;
    String dataHora;
}
