package br.mp.mprj.commons.model;


import lombok.*;
import org.joda.time.DateTime;

import java.util.List;
import java.util.Objects;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class DocumentoMni {

    /**Dados do cabeçalho do documento*/
    Integer nivelSigilo;
    String tipoDocumentoLocal;
    String descricao;
    String idDocumento;
    String tipoDocumento;
    DateTime dataHora;
    byte[] conteudo;
    List<String> assinaturas;

    /**Dados de 'outrosParametros'*/
    String destinatarioComunicao;
    String identificadorDocumento;
    String identificadorDocumentoPai;
    String volume;
    String numeroFolhaVirtual;
    String dataAberturaComunicacao;
    String dataResposta;
    String identificadorAviso;
    String protocoloPeticao;
    String statusComunicacao;
    String documentoEletronico;

}
