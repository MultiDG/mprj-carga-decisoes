package br.mp.mprj.commons.model;


import lombok.Builder;
import lombok.Getter;

@Builder
@Getter
public class ProcessoMni {
    String meioTramitacao;
}
