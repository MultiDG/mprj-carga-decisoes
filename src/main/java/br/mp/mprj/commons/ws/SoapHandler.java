package br.mp.mprj.commons.ws;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SoapHandler {

    private static final Logger LOGGER = LoggerFactory.getLogger(SoapHandler.class);

    public void handleRequest(String xml, String methodName, String className, String mniVersion, String idRequest) {
        String s = handle(xml, methodName, className, mniVersion, idRequest, false);
        LOGGER.info(s);
    }


    public void handleResponse(String xml, String methodName, String className, String mniVersion, String idRequest) {
        String s = handle(xml, methodName, className, mniVersion, idRequest, false);
        LOGGER.info(s);
    }

    public void handleException(Throwable e, String methodName, String className, String mniVersion, String idRequest) {
        String s = handle(e.getMessage(), methodName, className, mniVersion, idRequest, true);
        LOGGER.error(s);
    }

    String handle(String message, String methodName, String className, String mniVersion, String idRequest, boolean error) {
        StringBuilder builder = new StringBuilder();
        builder.append("\n");
        if(error) {
            builder.append("Problemas na requisicao: " + idRequest);
        } else {
            builder.append("Requisicao id: " + idRequest);
        }
        builder.append("\n");
        builder.append("Metodo :" + methodName);
        builder.append("\n");
        builder.append("Classe: " + className);
        builder.append("\n");
        builder.append("Versao Mni: " + mniVersion);
        builder.append("\n");
        builder.append(message);
        builder.append("\n");
        String s = builder.toString();
        return s;
    }

}
