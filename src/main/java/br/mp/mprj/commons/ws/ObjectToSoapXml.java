package br.mp.mprj.commons.ws;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.Marshaller;
import java.io.StringWriter;
import java.io.Writer;

public class ObjectToSoapXml {

    public String getXml(JAXBElement je) {
        try {
            if(je == null || je.getValue() == null) {
                return null;
            }
            JAXBContext jaxbCtx = JAXBContext.newInstance(je.getValue().getClass());
            Marshaller marshaller = jaxbCtx.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
            Writer writer = new StringWriter();
            marshaller.marshal(je,writer);
            String result = writer.toString();
            return result;
        } catch (Exception e) {
            return null;
        }
    }
}
