package br.mp.mprj.commons.ws;


import br.mp.mprj.commons.Constants;
import br.mp.mprj.commons.support.IdGenerator;
import br.mp.mprj.mni.tjrj.hml.v222.homologacao.*;
import org.springframework.stereotype.Component;

import javax.net.ssl.*;
import javax.xml.namespace.QName;
import java.net.MalformedURLException;
import java.net.Socket;
import java.net.URL;
import java.security.cert.CertificateException;

import static br.mp.mprj.commons.Constants.MNI_VERSION;

@Component
public class MniTjrjClient {

    ObjectToSoapXml objectToSoapXml;
    SoapHandler soapHandler;
    ObjectFactory objectFactory;
    ServicoIntercomunicacao222 service;
    IdGenerator generator = new IdGenerator();

    public MniTjrjClient() {
        this.objectFactory = new ObjectFactory();
        this.objectToSoapXml = new ObjectToSoapXml();
        this.soapHandler = new SoapHandler();
        /*Servico servico = new Servico();
        this.service = servico.getServicoIntercomunicacao();
        if(!servico.getWSDLDocumentLocation().toString().equals("http://wwwh1.tjrj.jus.br/HMNI/Servico.svc?wsdl")) {
            throw new RuntimeException("Illegal wsdl location " + servico.getWSDLDocumentLocation());
        }*/

    }

    private TipoConsultarTeorComunicacaoResposta consultarTeorComunicacao(TipoConsultarTeorComunicacao parameters) {
        String methodName = "consultarTeorComunicacao";
        String idRequest = generator.generate();
        parameters.setIdConsultante(Constants.USUARIO_HOMOLOGACAO);
        parameters.setSenhaConsultante(Constants.SENHA_HOMOLOGACAO);
        String xmlRequest = objectToSoapXml.getXml(objectFactory.createConsultarTeorComunicacao(parameters));
        soapHandler.handleRequest(xmlRequest, methodName, this.getClass().getName(), MNI_VERSION, idRequest);
        try {
            TipoConsultarTeorComunicacaoResposta resposta = service.consultarTeorComunicacao(parameters);
            String xmlResponse = objectToSoapXml.getXml(objectFactory.createConsultarTeorComunicacaoResposta(resposta));
            soapHandler.handleResponse(xmlResponse, methodName, this.getClass().getName(), MNI_VERSION, idRequest);
            return resposta;
        } catch (Throwable e) {
            soapHandler.handleException(e, methodName, this.getClass().getName(), MNI_VERSION, idRequest);
            throw e;
        }
    }

    public TipoEntregarManifestacaoProcessualResposta entregarManifestacaoProcessual(TipoEntregarManifestacaoProcessual parameters) throws MalformedURLException {
        String methodName = "entregarManifestacaoProcessual";
        String idRequest = generator.generate();
        parameters.setIdManifestante(Constants.USUARIO_HOMOLOGACAO);
        parameters.setSenhaManifestante(Constants.SENHA_HOMOLOGACAO);
        String xmlRequest = objectToSoapXml.getXml(objectFactory.createEntregarManifestacaoProcessual(parameters));
        soapHandler.handleRequest(xmlRequest, methodName, this.getClass().getName(), MNI_VERSION, idRequest);
        try{
            Servico servicoIntercomunicacao222_service = new Servico();
            TipoEntregarManifestacaoProcessualResposta resposta = servicoIntercomunicacao222_service.getServicoIntercomunicacao().entregarManifestacaoProcessual(parameters);
            String xmlResponse = objectToSoapXml.getXml(objectFactory.createEntregarManifestacaoProcessualResposta(resposta));
            soapHandler.handleResponse(xmlResponse, methodName, this.getClass().getName(), MNI_VERSION, idRequest);
            return resposta;
        } catch (Throwable e) {
            soapHandler.handleException(e, methodName, this.getClass().getName(), MNI_VERSION, idRequest);
            throw e;
        }
    }

    private Servico configuraAmbiente(TipoConsultarProcesso parameters, boolean producao, String idRequest) throws MalformedURLException {
        parameters.setIdConsultante(Constants.USUARIO_HOMOLOGACAO);
        String xmlRequest = objectToSoapXml.getXml(objectFactory.createConsultarProcesso(parameters));
        soapHandler.handleRequest(xmlRequest, Constants.CONSULTAR_PROCESSO, this.getClass().getName(), MNI_VERSION, idRequest);
        Servico servico;
        if (producao) {
            trustAllHosts();
            parameters.setSenhaConsultante(Constants.SENHA_PRODUCAO);
            servico = new Servico(
                    new URL("https://webserverseguro.tjrj.jus.br/MNI/Servico.svc?wsdl"),
                    new QName("http://www.cnj.jus.br/servico-intercomunicacao-2.2.2/", "Servico"));
        } else {
            parameters.setSenhaConsultante(Constants.SENHA_HOMOLOGACAO);
            servico = new Servico();
        }
        return servico;
    }

    public TipoConsultarProcessoResposta consultarProcesso(TipoConsultarProcesso parameters, boolean producao) throws MalformedURLException{
        String idRequest = generator.generate();
        try {

            Servico servico = configuraAmbiente(parameters, producao, idRequest);
            TipoConsultarProcessoResposta resposta = servico.getServicoIntercomunicacao().consultarProcesso(parameters);

            String xmlResponse = objectToSoapXml.getXml(objectFactory.createConsultarProcessoResposta(resposta));
            soapHandler.handleResponse(xmlResponse, Constants.CONSULTAR_PROCESSO, this.getClass().getName(), MNI_VERSION, idRequest);
            return resposta;

        } catch (Exception e) {
            soapHandler.handleException(e, Constants.CONSULTAR_PROCESSO, this.getClass().getName(), MNI_VERSION, idRequest);
            throw e;
        }
    }

    public void trustAllHosts() {
        try
        {
            TrustManager[] trustAllCerts = new TrustManager[]{
                    new X509ExtendedTrustManager()
                    {
                        @Override
                        public java.security.cert.X509Certificate[] getAcceptedIssuers()
                        {
                            return null;
                        }

                        @Override
                        public void checkClientTrusted(java.security.cert.X509Certificate[] certs, String authType)
                        {
                        }

                        @Override
                        public void checkServerTrusted(java.security.cert.X509Certificate[] certs, String authType)
                        {
                        }

                        @Override
                        public void checkClientTrusted(java.security.cert.X509Certificate[] xcs, String string, Socket socket) throws CertificateException
                        {

                        }

                        @Override
                        public void checkServerTrusted(java.security.cert.X509Certificate[] xcs, String string, Socket socket) throws CertificateException
                        {

                        }

                        @Override
                        public void checkClientTrusted(java.security.cert.X509Certificate[] xcs, String string, SSLEngine ssle) throws CertificateException
                        {

                        }

                        @Override
                        public void checkServerTrusted(java.security.cert.X509Certificate[] xcs, String string, SSLEngine ssle) throws CertificateException
                        {

                        }

                    }
            };

            SSLContext sc = SSLContext.getInstance("SSL");
            sc.init(null, trustAllCerts, new java.security.SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());

            // Create all-trusting host name verifier
            HostnameVerifier allHostsValid = new  HostnameVerifier()
            {
                @Override
                public boolean verify(String hostname, SSLSession session)
                {
                    return true;
                }
            };
            // Install the all-trusting host verifier
            HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    public String getXml(Object obj) {
        // xmls do metodo consultarTeorComunicacao
        if(obj instanceof TipoConsultarTeorComunicacao) {
            return objectToSoapXml.getXml(objectFactory.createConsultarTeorComunicacao((TipoConsultarTeorComunicacao) obj));
        }
        if(obj instanceof TipoConsultarTeorComunicacaoResposta) {
            return objectToSoapXml.getXml(objectFactory.createConsultarTeorComunicacaoResposta((TipoConsultarTeorComunicacaoResposta) obj));
        }
        // xmls do metodo entregarManifestacaoProcessual
        if(obj instanceof TipoEntregarManifestacaoProcessual) {
            return objectToSoapXml.getXml(objectFactory.createEntregarManifestacaoProcessual((TipoEntregarManifestacaoProcessual) obj));
        }
        if(obj instanceof TipoEntregarManifestacaoProcessualResposta) {
            return objectToSoapXml.getXml(objectFactory.createEntregarManifestacaoProcessualResposta((TipoEntregarManifestacaoProcessualResposta) obj));
        }
        return "";
    }
}
