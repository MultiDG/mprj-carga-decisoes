package br.mp.mprj.commons;

import lombok.Getter;
import lombok.Setter;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

@Service
@Setter
@Getter
@Lazy
public class AmbienteConfig {

    String caminhoArquivo;
    String ambiente;
    public Connection getConexao() throws Exception {
        return this.obterSqlSession(this.ambiente);
    }

    public Connection getConexao(String ambiente) throws Exception {
        return this.obterSqlSession(ambiente);
    }

    protected Connection obterSqlSession(String ambiente) throws Exception {
        String url = null;
        String user = null;
        String password = null;
        Connection connection = null;
        switch (ambiente){
            case "DESENVOLVIMENTO":
                url = Constants.ORACLE_DESENV_URL;
                user = Constants.ORACLE_DESENV_USER;
                password = Constants.ORACLE_DESENV_PASSWORD;
                break;
            case "PRODUÇÃO":
                url = Constants.ORACLE_PROD_URL;
                user = Constants.ORACLE_PROD_USER;
                password = Constants.ORACLE_PROD_PASSWORD;
                break;
            case "CORREÇÃO":
                url = Constants.ORACLE_CORR_URL;
                user = Constants.ORACLE_CORR_USER;
                password = Constants.ORACLE_CORR_PASSWORD;
                break;
        }
        try {
            connection = DriverManager.getConnection(url, user, password);
        }catch (SQLException e){
            e.getCause();
        }
        return connection;
    }
}
