package br.mp.mprj.commons;

import java.util.regex.Pattern;

public class Constants {

    public static final String CONSULTAR_PROCESSO = "consultarProcesso";
    public static final String MNI_DATE_PATTERN = "yyyyMMddHHmmss";
    public static final String USUARIO_HOMOLOGACAO = "28305936000140";
    public static final String SENHA_HOMOLOGACAO = "DFHF52";
    public static final String SENHA_PRODUCAO = "f80#71b=f5";
    public static final String DESTINATARIO_COMUNICACAO = "DESTINATARIO_COMUNICACAO";
    public static final String IDENTIFICADOR_DOCUMENTO = "IDENTIFICADOR_DOCUMENTO";
    public static final String NUMERO_FOLHA_VIRTUAL = "NUMERO_FOLHA_VIRTUAL";
    public static final String PROTOCOLO_PETICAO = "PROTOCOLO_PETICAO";
    public static final String STATUS_COMUNICACAO = "STATUS_COMUNICACAO";
    public static final String DATA_ABERTURA_COMUNICACAO = "DATA_ABERTURA_COMUNICACAO";
    public static final String DATA_RESPOSTA = "DATA_RESPOSTA";
    public static final String IDENTIFICADOR_AVISO = "IDENTIFICADOR_AVISO";
    public static final String IDENTIFICADOR_DOCUMENTO_PAI = "IDENTIFICADOR_DOCUMENTO_PAI";
    public static final String MEIO_TRAMITACAO = "MEIO_TRAMITACAO";
    public static final String INSTANCIA = "INSTANCIA";
    public static final String IMPAR = "IMPAR";
    public static final String PAR = "PAR";

    // mni version
    public static final String MNI_VERSION = "2.2.2";
    public static final Pattern NUMEROCNJ_PATTERN = Pattern.compile("(\\d{7}-\\d{2}\\.\\d{4}\\.8\\.19\\.\\d{4})");


    public static final String ORACLE_DESENV_URL = "jdbc:oracle:thin:@(DESCRIPTION=(LOAD_BALANCE=on)(ADDRESS=(PROTOCOL=TCP)(HOST=exa-scan.pgj.rj.gov.br) (PORT=1521))(CONNECT_DATA=(SERVICE_NAME=desenv)))";
    public static final String ORACLE_DESENV_USER = "TJRJ";
    public static final String ORACLE_DESENV_PASSWORD = "TJRJ4521W";
}
