package br.mp.mprj.commons;

import br.mp.mprj.commons.model.DocumentoMni;
import br.mp.mprj.commons.model.ProcessoMni;
import br.mp.mprj.mni.tjrj.hml.v222.homologacao.TipoCabecalhoProcesso;
import br.mp.mprj.mni.tjrj.hml.v222.homologacao.TipoConsultarProcessoResposta;
import br.mp.mprj.mni.tjrj.hml.v222.homologacao.TipoDocumento;
import br.mp.mprj.mni.tjrj.hml.v222.homologacao.TipoParametro;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ConverterMni {


    public DocumentoMni documentoMni(TipoDocumento response){
         DocumentoMni documento = new DocumentoMni();
              for (TipoParametro outroParametro : response.getOutroParametro()) {
                  switch (outroParametro.getNome()){
                        case Constants.DESTINATARIO_COMUNICACAO :
                            documento.setDestinatarioComunicao(outroParametro.getValor() == "" ? null : outroParametro.getValor());
                            break;
                        case Constants.IDENTIFICADOR_DOCUMENTO :
                            documento.setIdentificadorDocumento(outroParametro.getValor() == "" ? null : outroParametro.getValor());
                            break;
                        case Constants.IDENTIFICADOR_DOCUMENTO_PAI :
                            documento.setIdentificadorDocumentoPai(outroParametro.getValor() == "" ? null : outroParametro.getValor());
                            break;
                        case Constants.IDENTIFICADOR_AVISO :
                            documento.setIdentificadorAviso(outroParametro.getValor() == "" ? null : outroParametro.getValor());
                            break;
                        case Constants.DATA_RESPOSTA :
                            documento.setDataResposta(outroParametro.getValor() == "" ? null : outroParametro.getValor());
                            break;
                        case Constants.DATA_ABERTURA_COMUNICACAO :
                            documento.setDataAberturaComunicacao(outroParametro.getValor() == "" ? null : outroParametro.getValor());
                            break;
                        case Constants.NUMERO_FOLHA_VIRTUAL :
                            documento.setNumeroFolhaVirtual(outroParametro.getValor() == "" ? null : outroParametro.getValor());
                            break;
                        case Constants.PROTOCOLO_PETICAO :
                            documento.setProtocoloPeticao(outroParametro.getValor() == "" ? null : outroParametro.getValor());
                            break;
                        case Constants.STATUS_COMUNICACAO :
                            documento.setStatusComunicacao(outroParametro.getValor() == "" ? null : outroParametro.getValor());
                            break;
                      case Constants.MEIO_TRAMITACAO :
                            documento.setDocumentoEletronico(outroParametro.getValor() == "" ? null : outroParametro.getValor());
                            break;
                    }
                }
                documento.setNivelSigilo(response.getNivelSigilo());
                documento.setTipoDocumento(response.getTipoDocumento());
                documento.setTipoDocumentoLocal(response.getTipoDocumentoLocal());
                documento.setDescricao(response.getDescricao());
                documento.setIdDocumento(response.getIdDocumento());
                documento.setIdentificadorDocumento(response.getIdDocumento());
                documento.setDataHora(DateTime.parse(response.getDataHora(), DateTimeFormat.forPattern(Constants.MNI_DATE_PATTERN)));
                documento.setConteudo(response.getConteudo());
        return documento;
    }

    public List<DocumentoMni> documentosMni(TipoConsultarProcessoResposta response){
        List<DocumentoMni> documentos = new ArrayList<>();
        if(response.getProcesso() != null && response.getProcesso().getDocumento() != null && response.isSucesso()){
            for(TipoDocumento tipoDocumento : response.getProcesso().getDocumento()){
                DocumentoMni documento = new DocumentoMni();
                for (TipoParametro outroParametro : tipoDocumento.getOutroParametro()) {
                    switch (outroParametro.getNome()){
                        case Constants.DESTINATARIO_COMUNICACAO :
                            documento.setDestinatarioComunicao(outroParametro.getValor() == "" ? null : outroParametro.getValor());
                            break;
                        case Constants.IDENTIFICADOR_DOCUMENTO :
                            documento.setIdentificadorDocumento(outroParametro.getValor() == "" ? null : outroParametro.getValor());
                            break;
                        case Constants.IDENTIFICADOR_DOCUMENTO_PAI :
                            documento.setIdentificadorDocumentoPai(outroParametro.getValor() == "" ? null : outroParametro.getValor());
                            break;
                        case Constants.IDENTIFICADOR_AVISO :
                            documento.setIdentificadorAviso(outroParametro.getValor() == "" ? null : outroParametro.getValor());
                            break;
                        case Constants.DATA_RESPOSTA :
                            documento.setDataResposta(outroParametro.getValor() == "" ? null : outroParametro.getValor());
                            break;
                        case Constants.DATA_ABERTURA_COMUNICACAO :
                            documento.setDataAberturaComunicacao(outroParametro.getValor() == "" ? null : outroParametro.getValor());
                            break;
                        case Constants.NUMERO_FOLHA_VIRTUAL :
                            documento.setNumeroFolhaVirtual(outroParametro.getValor() == "" ? null : outroParametro.getValor());
                            break;
                        case Constants.PROTOCOLO_PETICAO :
                            documento.setProtocoloPeticao(outroParametro.getValor() == "" ? null : outroParametro.getValor());
                            break;
                        case Constants.STATUS_COMUNICACAO :
                            documento.setStatusComunicacao(outroParametro.getValor() == "" ? null : outroParametro.getValor());
                            break;
                        case Constants.MEIO_TRAMITACAO :
                            documento.setDocumentoEletronico(outroParametro.getValor() == "" ? null : outroParametro.getValor());
                            break;
                    }
                }
                documento.setNivelSigilo(tipoDocumento.getNivelSigilo());
                documento.setTipoDocumento(tipoDocumento.getTipoDocumento());
                documento.setTipoDocumentoLocal(tipoDocumento.getTipoDocumentoLocal());
                documento.setDescricao(tipoDocumento.getDescricao());
                documento.setIdDocumento(tipoDocumento.getIdDocumento());
                documento.setIdentificadorDocumento(tipoDocumento.getIdDocumento());
                documento.setDataHora(DateTime.parse(tipoDocumento.getDataHora(), DateTimeFormat.forPattern(Constants.MNI_DATE_PATTERN)));
                documento.setConteudo(tipoDocumento.getConteudo());
                documentos.add(documento);
            }
        }
        return documentos;
    }

    public ProcessoMni dadosBasicos(TipoCabecalhoProcesso cabecalhoProcesso){
        ProcessoMni.ProcessoMniBuilder processoMni = null;
        for (TipoParametro parametro : cabecalhoProcesso.getOutroParametro()) {
            if(parametro.getNome().equals(Constants.MEIO_TRAMITACAO)){
                processoMni = ProcessoMni.builder().meioTramitacao(parametro.getValor());
            }
        }
      return processoMni.build();
    }

    public String toSituacaoMgpe(String situacaoMni){
        String situacaoMgpe = null;
        if(situacaoMni != null){
            switch (situacaoMni){
                case "Intimado": situacaoMgpe = "PENDENTE";
                    break;
                case "Intimado tacitamente": situacaoMgpe = "RECEBIDA_TACITA";
                    break;
                case "Recebida": situacaoMgpe = "RESPONDIDA";
                    break;
                case "Respondida": situacaoMgpe = "RECEBIDA";
                    break;
            }
        }
        return situacaoMgpe;
    }


    public DateTime toDateMgpe(String dataMni){
       return DateTime.parse(dataMni, DateTimeFormat.forPattern(Constants.MNI_DATE_PATTERN));
    }
}
