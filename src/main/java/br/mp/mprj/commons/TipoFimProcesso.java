package br.mp.mprj.commons;

import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Component;

@Component
@Getter
@Setter
public class TipoFimProcesso {

   private String tipo;

}
