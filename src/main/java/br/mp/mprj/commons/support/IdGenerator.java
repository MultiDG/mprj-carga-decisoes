package br.mp.mprj.commons.support;

import java.util.UUID;

public class IdGenerator {
    public String generate() {
        return UUID.randomUUID().toString();
    }
}
