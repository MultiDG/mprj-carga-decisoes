package br.mp.mprj.commons.relational;

import lombok.Builder;
import lombok.Getter;
import org.joda.time.DateTime;

import java.util.Date;

@Getter
@Builder(toBuilder = true)
public class ProcessoDiarioOficialRelational {

    String numeroCNJ;
    java.sql.Date dataDiarioOficial;
    String capitalAnterior;
    int pagina;
    byte[] documento;
    byte[] documentoRejeitado;
    java.sql.Date dataInclusaoDocumento;
    String descricaoMni;
    java.sql.Date dataInclusaoMni;
    java.sql.Date dataInclusao;
    String xmlResponse;
    String isEletronico;
    Integer instancia;

}
