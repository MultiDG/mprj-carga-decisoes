package br.mp.mprj.commons.relational;

import lombok.Getter;
import org.joda.time.DateTime;

@Getter
public class IntimacaoRespondidaRelational {
    String idAviso;
    String numeroCNJ;
    DateTime dataResposta;
}
