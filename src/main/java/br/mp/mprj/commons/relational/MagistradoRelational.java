package br.mp.mprj.commons.relational;

import lombok.Getter;

@Getter
public class MagistradoRelational {
    String nome;
    String nomeCorrigido;
}
