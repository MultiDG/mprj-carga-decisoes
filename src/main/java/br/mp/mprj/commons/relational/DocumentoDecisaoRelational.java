package br.mp.mprj.commons.relational;

import lombok.Builder;
import lombok.Getter;

@Builder
@Getter
public class DocumentoDecisaoRelational {

    Long id;
    String tituloPdf;
    byte[] pdf;

}
