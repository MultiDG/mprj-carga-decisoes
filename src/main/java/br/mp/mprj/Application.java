package br.mp.mprj;


import br.mp.mprj.cargadecisoes.DocumentoDecisaoDO;
import br.mp.mprj.commons.AmbienteConfig;
import br.mp.mprj.commons.TipoFimProcesso;
import br.mp.mprj.view.CargaDecisoesForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import javax.swing.*;
import java.awt.*;


@SpringBootApplication
public class Application implements CommandLineRunner {
    public static void main(String[] args) {
        SpringApplication app = new SpringApplication(Application.class);
        app.setHeadless(false);
        app.run(args);
    }

    @Autowired
    DocumentoDecisaoDO documentoDecisaoDO;

    @Autowired
    AmbienteConfig config;

    @Autowired
    TipoFimProcesso tipoFimProcesso;

    @Bean
    public CargaDecisoesForm cargaDecisoesForm() {
        return new CargaDecisoesForm(documentoDecisaoDO, config, tipoFimProcesso);
    }

    @Autowired
    CargaDecisoesForm cargaDecisoesForm;

    @Override
    public void run(String... strings) throws Exception {
        EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    JFrame frame = new JFrame("CargaDecisoesForm");
                    frame.setContentPane(cargaDecisoesForm.panelView);
                    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                    frame.pack();
                    frame.setSize(600,300);
                    frame.setLocationRelativeTo(null);
                    frame.setVisible(true);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }
}
