package br.mp.mprj.view;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import javax.swing.*;
import java.awt.*;

public class AppPrincipalForm implements CommandLineRunner {

    @Autowired
    CargaDecisoesForm cargaDecisoesForm;

    @Override
    public void run(String... arg0) throws Exception {
        EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    JFrame frame = new JFrame("CargaDecisoesForm");
                    frame.setContentPane(cargaDecisoesForm.panelView);
                    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                    frame.pack();
                    frame.add(new Label("teste"));
                    frame.setSize(600,300);
                    frame.setLocationRelativeTo(null);
                    frame.setVisible(true);
                    //we'll use an indeterminate progress bar

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }
}
